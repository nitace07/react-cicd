import React, { Suspense } from 'react'
import './App.css';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

const Home = React.lazy(() => import('./Pages/Home'));

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          {/* Home Component */}
          <Route
            exact
            path="/"
            render={
              // Fallback should be the css skeleton
              () => <Suspense fallback={
                <div>
                  Loading Home...
                </div>}>
                <Home />
              </Suspense>} />

          {/* // */}
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
