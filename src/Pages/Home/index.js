import React, { Suspense, useState } from 'react';
import './style.css'

// Page Components
const Navbar = React.lazy(() => import('../../Components/Navbar'))
const Carousel = React.lazy(() => import('../../Components/Carousel'))
const ADs = React.lazy(() => import('../../Components/ADs'))
const Inventory = React.lazy(() => import('../../Components/Inventory/Inventory'))

const index = () => {
    return (
        <div className="home">
            {/* topbar */}
            <Suspense fallback={<div>Nav Loading...</div>}>
                <Navbar />
            </Suspense>

            {/* slider carousel */}
            <Carousel />

        </div>
    );
}

export default index;
