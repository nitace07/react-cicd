import React from 'react';
import './style.css';

// material-ui
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import Button from '@material-ui/core/Button';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';

const index = () => {
    return (
        <div className="navbar">
            {/* brand */}
            <div className="navbar-brand">Nitesh CI/CD </div>

            {/* menu items */}
            <div className="navbar-menu-items">
                <ul>
                    <li>s3</li>
                    <li>EC2</li>
                    <li>lambda</li>
                    <li>RDS</li>
                    <li>Kubernetes</li>
                    <li>ECS</li>
                    <li>Fargate</li>
                </ul>
            </div>

            {/* checkouts */}
            <div className="navbar-checkouts">
                <Button>
                    <ShoppingCartIcon />
                </Button>
                <Button>
                    <AccountCircleIcon />
                </Button>
            </div>
        </div>
    );
}

export default index;
