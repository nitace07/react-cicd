import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

const ProductCard = React.lazy(() => import('../ProductCard/ProductCard'))

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
        display: "block",
        margin: "0 auto"
    },
}));

export default function Inventory() {
    const classes = useStyles();
    const [value, setValue] = React.useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    return (
        <div className={classes.root}>
            <Tabs value={value} onChange={handleChange} aria-label="simple tabs example" centered>
                <Tab label="Beads" {...a11yProps(0)} />
                <Tab label="Plants" {...a11yProps(1)} />
                <Tab label="Shoes" {...a11yProps(2)} />
            </Tabs>

            <TabPanel value={value} index={0}>
                <ul className="items">
                    <Suspense fallback={
                        <div>Product Card Loading...</div>
                    }>
                        <ProductCard />
                    </Suspense>

                    <Suspense fallback={
                        <div>Product Card Loading...</div>
                    }>
                        <ProductCard />
                    </Suspense>

                    <Suspense fallback={
                        <div>Product Card Loading...</div>
                    }>
                        <ProductCard />
                    </Suspense>

                    <Suspense fallback={
                        <div>Product Card Loading...</div>
                    }>
                        <ProductCard />
                    </Suspense>

                    <Suspense fallback={
                        <div>Product Card Loading...</div>
                    }>
                        <ProductCard />
                    </Suspense>

                    <Suspense fallback={
                        <div>Product Card Loading...</div>
                    }>
                        <ProductCard />
                    </Suspense>

                    <Suspense fallback={
                        <div>Product Card Loading...</div>
                    }>
                        <ProductCard />
                    </Suspense>

                    <Suspense fallback={
                        <div>Product Card Loading...</div>
                    }>
                        <ProductCard />
                    </Suspense>
                </ul>
            </TabPanel>
            <TabPanel value={value} index={1}>
                <ul className="items">
                    <Suspense fallback={
                        <div>Product Card Loading...</div>
                    }>
                        <ProductCard />
                    </Suspense>

                    <Suspense fallback={
                        <div>Product Card Loading...</div>
                    }>
                        <ProductCard />
                    </Suspense>

                    <Suspense fallback={
                        <div>Product Card Loading...</div>
                    }>
                        <ProductCard />
                    </Suspense>

                    <Suspense fallback={
                        <div>Product Card Loading...</div>
                    }>
                        <ProductCard />
                    </Suspense>

                    <Suspense fallback={
                        <div>Product Card Loading...</div>
                    }>
                        <ProductCard />
                    </Suspense>

                    <Suspense fallback={
                        <div>Product Card Loading...</div>
                    }>
                        <ProductCard />
                    </Suspense>

                    <Suspense fallback={
                        <div>Product Card Loading...</div>
                    }>
                        <ProductCard />
                    </Suspense>

                    <Suspense fallback={
                        <div>Product Card Loading...</div>
                    }>
                        <ProductCard />
                    </Suspense>
                </ul>
            </TabPanel>
            <TabPanel value={value} index={2}>
                <ul className="items">
                    <Suspense fallback={
                        <div>Product Card Loading...</div>
                    }>
                        <ProductCard />
                    </Suspense>

                    <Suspense fallback={
                        <div>Product Card Loading...</div>
                    }>
                        <ProductCard />
                    </Suspense>

                    <Suspense fallback={
                        <div>Product Card Loading...</div>
                    }>
                        <ProductCard />
                    </Suspense>

                    <Suspense fallback={
                        <div>Product Card Loading...</div>
                    }>
                        <ProductCard />
                    </Suspense>

                    <Suspense fallback={
                        <div>Product Card Loading...</div>
                    }>
                        <ProductCard />
                    </Suspense>

                    <Suspense fallback={
                        <div>Product Card Loading...</div>
                    }>
                        <ProductCard />
                    </Suspense>

                    <Suspense fallback={
                        <div>Product Card Loading...</div>
                    }>
                        <ProductCard />
                    </Suspense>

                    <Suspense fallback={
                        <div>Product Card Loading...</div>
                    }>
                        <ProductCard />
                    </Suspense>
                </ul>
            </TabPanel>
        </div>
    );
}