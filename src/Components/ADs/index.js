import React from 'react';
import { Link } from 'react-router-dom'

const index = () => {
    return (
        <>
            {/* single ad */}
            <div className="home-single-ad">
                <div className="ad-content">
                    <h1>
                        Peace Lily
                    </h1>
                    <p>
                        Lorem ipsum dolor, sit amet consectetur adipisicing elit. Sunt laborum totam quidem alias, eveniet saepe impedit. Amet neque eaque in vel iusto! Perferendis labore ducimus nisi error inventore. Dolorem, praesentium!
                    </p>

                    <Link to="#">Purchase Now</Link>
                </div>

                <div className="ad-image">
                    <img src="https://images.unsplash.com/photo-1607163365613-c281acde5012?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80" alt="Peace Lily" />
                </div>
            </div>

            {/* dual ad */}

            {/* need to make custom hover cursor */}
            <div className="home-dual-ad">
                <div className="ad-beads">
                    <Link to="#">
                        <img 
                            src="https://images.unsplash.com/photo-1515562141207-7a88fb7ce338?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80" 
                            alt="ad beads" 
                        />
                    </Link>
                </div>

                <div className="ad-shoes">
                    <Link to="#">
                        <img 
                            src="https://images.unsplash.com/photo-1542291026-7eec264c27ff?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80" 
                            alt="nike shoes" 
                        />
                    </Link>
                </div>
            </div>
        </>
    );
}

export default index;
